//Lorsque document est chargé effectuer JS
$(document).ready(function(){

    //Création de la nouvelle instance de Calculator()
    var calc = new Calculator();

    //Appel de la methode d'initialisation
    calc.run();

});

//Création de la classe Calculator()
function Calculator()
{
    //Création des attributs dans la classe Calculator()
    that = this,

    //Vise le champ input avec id number
    this.field = "input#number",

    //Bouton déclencheur des actions
    this.button = "#body-calculator .buttons",
    this.buttonCe = "#clear",
    this.init = false,

    //Création de 4 méthodes
    this.run = function()
    {
        console.log('JS Load !');

        //Lorsqu'un bouton est cliqué alors...
        $(this.button).click(function(){
            //Initialisation de la valeur à cette instance jQuery = à ce clic précis récupération de la valeur dans la <div>
            var value = $(this).html();

            //Pour ne pas confondre avec le this de jQuery utilisation de l'attribut that defini plus haut
            if(that.init == false)
            {
                //Champ avec valeur par défaut vide
                $(that.field).val("");
                that.init = true;
            }

            if(value != "=")
            //Affichage dans l'input de la valeur cliquée
                $(that.field).val($(that.field).val() + value);

            that.dispatcher(value);
        });
    },

    this.dispatcher = function(value)
    {
        //Orientation vers l'opération a effectuer => dans notre cas uniquement additionner
        if($(this.field).val().indexOf("+") != -1)
            this.operation(value, "+");

        //...Et ainsi de suite pour les autres types d'opréation "-", "/", "*"
    },

    this.operation = function(value, symbol)
    {
        //Séparation des valeurs
        var numbers = $(this.field).val().split(symbol),
            result;

            console.log(numbers);

        if(symbol == "+")
            //Utilisation de parseFloat afin de ne pas confondre la concaténation
            result = parseFloat(numbers[0]) + parseFloat(numbers[1]);

        //Résultat à deux décimals
        result = Math.round((result) *100) / 100;

        //Affichage du résultat
        if(numbers.length > 1)
        {
            if(value == "=")
                $(this.field).val(result);
                else if(numbers.length > 2)
                    $(this.field).val(result);
        }
    },

    //Essaie d'un bouton pour revenir à zéro
    this.clear = function()
    {
        $(this.buttonCe).click(function(){

            $(that.field).empty();
        });
    };
}