# Test-ArteStudio

Développer une calculatrice permettant d'additionner deux nombres. Elle répondra aux critères suivants :
- séparation du code entre un client web, un serveur distant
- le calcul doit s'effectuer sur le serveur distant et prendre quelques secondes (simuler un temps de calcul)

Pour information, seront jugés, entre autre : la qualité du code, la facilité d'installation et d'execution, la documentation et l'ergonomie de l'ensemble. Pour le front et le back, le choix des langages et framework est libre.
